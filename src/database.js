const mongoose = require("mongoose");

// Parche por que el archivo de variables de netorno no funciono
// mongodb+srv://bermoz:b3rmoz2020@cluster0-lk73x.mongodb.net/test?retryWrites=true&w=majority
const NOTES_APP_MONGODB_HOST="bermoz:b3rmoz2020@cluster0-lk73x.mongodb.net";
const NOTES_APP_MONGODB_DATABASE="comisionverdad?retryWrites=true&w=majority";
const NODE_ENV="product";

// const { NOTES_APP_MONGODB_HOST, NOTES_APP_MONGODB_DATABASE } = process.env;
 const MONGODB_URI = `mongodb+srv://${NOTES_APP_MONGODB_HOST}/${NOTES_APP_MONGODB_DATABASE}`;
//const MONGODB_URI = `mongodb+srv://bermoz:b3rmoz2020@cluster0-lk73x.mongodb.net/test?retryWrites=true&w=majority`;

mongoose
  .connect(MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  })
  .then(db => console.log("DB esta conectada"))
  .catch(err => console.error(err));
