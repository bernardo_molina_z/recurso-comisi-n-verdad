const recursosCtrl = {};

// Models
const Recurso = require("../models/Recurso");

recursosCtrl.renderFormRecurso = (req, res) => {
  res.render("recursos/new-recursos");
};

recursosCtrl.createNewRecurso = async (req, res) => {
  const { titulo, claves, descripcion, fuente, tiporecurso,
  fechaini,
  fechafin,
  ubicacion,
  longitud,
  latitud,
  direccion,
  ciudad,
  departamento,
  codigopostal,
  pais } = req.body;
  const errors = [];
  if (!titulo) {
    errors.push({ text: "Por favor escriba un título." });
  }
  if (!claves) {
    errors.push({ text: "Por favor escriba las claves separadas por comas." });
  }
  if (!descripcion) {
    errors.push({ text: "Por favor escriba una descripción." });
  }
  if (!fuente) {
    errors.push({ text: "Por favor escriba un título." });
  }
  if (errors.length > 0) {
    res.render("recursos/new-recursos", {
      errors,
      titulo,
      claves,
      descripcion,
      fuente,
      tiporecurso,
      fechaini,
      fechafin,
      ubicacion,
      longitud,
      latitud,
      direccion,
      ciudad,
      departamento,
      codigopostal,
      pais
    });
  } else {
    const newRecurso = new Recurso({ titulo, claves, descripcion, fuente,
    tiporecurso,
    fechaini,
    fechafin,
    ubicacion,
    longitud,
    latitud,
    direccion,
    ciudad,
    departamento,
    codigopostal,
    pais });
    await newRecurso.save();
    req.flash("success_msg", "Recurso añadido correctamente.");
    res.redirect("/recursos");
  }
};

recursosCtrl.renderQueryRecursos = async (req, res) => {
  const recursos = await Recurso.find();
  res.render("recursos/all-recursos", { recursos });
};

recursosCtrl.renderQueryRecursos2 = async (req, res) => {
  const recursos = await Recurso.find();
  res.render("recursos/listar-recursos", { recursos });
};

recursosCtrl.renderEditForm = async (req, res) => {
  const recurso = await Recurso.findById(req.params.id);
  res.render("recursos/edit-recursos", { recurso });
};

recursosCtrl.updateRecursos = async (req, res) => {
  const { titulo, descripcion } = req.body;
  await Recurso.findByIdAndUpdate(req.params.id, { titulo, claves, descripcion,
  fuente,
  tiporecurso,
  fechaini,
  fechafin,
  ubicacion,
  longitud,
  latitud,
  direccion,
  ciudad,
  departamento,
  codigopostal,
  pais });
  req.flash("success_msg", "Recurso actualizado correctamente.");
  res.redirect("/recursos");
};

recursosCtrl.deleteRecurso = async (req, res) => {
  await Recurso.findByIdAndDelete(req.params.id);
  req.flash("success_msg", "Recurso eliminado correctamente.");
  res.redirect("/recursos");
};

module.exports = recursosCtrl;
