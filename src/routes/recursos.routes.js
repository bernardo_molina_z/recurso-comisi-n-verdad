const express = require("express");
const router = express.Router();

// Controller
const {
  renderFormRecurso,
  createNewRecurso,
  renderQueryRecursos,
  renderQueryRecursos2,
  renderEditForm,
  updateRecursos,
  deleteRecurso
} = require("../controllers/recursos.controller");

// Helpers
const { isAuthenticated } = require("../helpers/auth");

// Nuevo Recurso
router.get("/recursos/add", isAuthenticated, renderFormRecurso);

router.post("/recursos/new-recursos", isAuthenticated, createNewRecurso);

// Get All Recursos
router.get("/recursos", isAuthenticated, renderQueryRecursos);

// Listar All Recursos
router.get("/recursos/listar-recursos", isAuthenticated, renderQueryRecursos2);

// Edit Recursos
router.get("/recursos/edit/:id", isAuthenticated, renderEditForm);

// Update Recursos
router.put("/recursos/actualizar-recurso/:id", isAuthenticated, updateRecursos);

// Delete Recursos
router.delete("/recursos/delete/:id", isAuthenticated, deleteRecurso);

module.exports = router;
