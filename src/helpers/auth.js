const helpers = {};

helpers.isAuthenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  }
  req.flash('error_msg', 'Usuario No autorizado.');
  res.redirect('/users/signin');
};

module.exports = helpers;
