const {
  Schema,
  model
} = require("mongoose");
/*
•	Título: Cadena de texto con el nombre dado a un recurso.
•	Claves: Lista de cadenas de texto con los temas del recurso.
•	Descripción: Cadena de texto resumen de un documento o una descripción en caso de ser un documento visual.
•	Fuente: Cadena de texto identificador único del recurso.
•	Tipo del Recurso: Cadena de texto con categoría del recurso. Use el siguiente dominio en este caso: Testimonio, Informe, Caso.
•	Cobertura: Combinación entre un rango de fechas y una ubicación.
*/
const RecursoSchema = new Schema({
      titulo: {
        type: String,
        required: true
      },
      claves: {
        type: String,
        required: true
      },
      descripcion: {
        type: String,
        required: true
      },
      fuente: {
        type: String,
        required: true
      },
      tiporecurso: {
        type: String,
        required: true
      },
      fechaini: {
        type: Date,
        required: true
      },
      fechafin: {
        type: Date,
        required: true
      },
      ubicacion: {
        type: String,
        required: true
      },
      latitud: {
        type: String,
        required: true
      },
      longitud: {
        type: String,
        required: true
      },
      direccion: {
        type: String,
        required: true
      },
      ciudad: {
        type: String,
        required: true
      },
      departamento: {
        type: String,
        required: true
      },
      codigopostal: {
        type: String,
        required: true
      },
      pais: {
        type: String,
        required: true
      }},
      {
        timestamps: true
      });

    module.exports = model("Recurso", RecursoSchema);
