# Recursos App con Tecnologia MERN  NodeJS, ExpressJS, ReactJS, Mongodb
Aplicación DEMOSTRACIÓN RECURSOS COMISION DELA VERDAD

![](docs/comisionverdad.png)

Esta es una simple aplicación para manejo de recursos documentales desarrollado con Tecnologia MERN Stack(MongoDB, Express, ReactJS y NodeJS).

![](docs/mern_logo.png)

Con esta aplicación puede:
- crear/consultar/Actualizar/Borrar Recursos de la comisión de la verdad
- Permite a un usuario iniciar sesión y guardar recursos documentales usando el estándar Dublin Core.

Usaremos el siguiente estándar para los metadatos:

Título: Cadena de texto con el nombre dado a un recurso.
Claves: Lista de cadenas de texto con los temas del recurso.
Descripción: Cadena de texto resumen de un documento o una descripción en caso de ser un documento visual.
Fuente: Cadena de texto identificador único del recurso.
Tipo del Recurso: Cadena de texto con categoría del recurso. Use el siguiente dominio en este caso: Testimonio, Informe, Caso.
Cobertura: Combinación entre un rango de fechas y una ubicación.

ADICIONALMENTE
- crear/consultar/Actualizar/Borrar Notas de Trabajo
- Permite a un usuario iniciar sesión y guardar sus notas personales de trabajo


# Screenshot
Esta es una vista de la pagina de inicio de la aplicación, luego de que la accedas en la URL donde sea instalada y configurada.

![](docs/presenta.png)

Al Hacer click sobre el menu en el enlace acerca, podras visualizar una pagina con informacion general del aplicativo; tecnologia, Generalidad, Autor. como se ve en la imagen.

![](docs/acerca.png)

Para poder usar el aplicativo primero deberas registrarte como usuario, proporcionando la informacion: Nombre completo del usuario, Correo Electronico, Contraseña, tal como se ve en la imagen a continuación.

![](docs/registro_usuario.png)

Una vez creado tu usuario valido, podras acceder al aplicativo usando tu correo lectronico como usuario y la contraseña que registraste.

![](docs/login.png)

Al acceder al aplicativo podras crear registros con la informacion y datos definidos en el estandar para cada uno de los recursos. 

![](docs/ficha2_recursos.png)

A medida que vas agregando recursos podras ir visualizando los diferentes registros creados en formato ficha de recursos o en un listado de recursos.
Tendras forma de commutar entre los dos modos de visualizar vista ficha o vista listado

Vista ficha

![](docs/ficha_recursos.png)

Vista listado

![](docs/lista_recursos.png)

En ambos Casos dispondras de botones para editar o borrar los registros de los recursos creados.
Para crear nuevos recursos dando click en el menu superior derecho Recursos donde encontras los enlaces para las funcionalidades de crear nuevos recursos.

![](docs/menu_recurso.png)

Tambien podras salir del aplicativo web.

Ademas tambien podras contrar con las funcionalidades de registro de notas de trabajo.

![](docs/notas.png)

-----------------------------------------------------------------------------------

# Variables de Entorno necesarias
Esta aplicación necesita las siguientes variables de entorno
debes registrar esta informacion modificando el archivo de configuracion con tus propios parametros.

* `NOTES_APP_MONGODB_HOST` Esta es la Mongodb URI cadena de texto (string)
* `NOTES_APP_MONGODB_DATABASE` nombre de la base de datos Mongodb 
* `NODE_ENV` node entorno

# Base de datos 
El aplicativo usa una base de datos MONGODB con  tres colecciones: 

users -> nombre de usuario, correo electronico, contrase, fecha de creacion, fecha de ultima actualizacion.
notes -> para guardar la informacion de las notas de trabajo con los datos titulo de la nota, descripcion de la nota, usuario autor, fecha de creacion, fecha de ultima actualizacion.
recursos -> Título: Cadena de texto con el nombre dado a un recurso.
Claves: Lista de cadenas de texto con los temas del recurso.
Descripción: Cadena de texto resumen de un documento o una descripción en caso de ser un documento visual.
Fuente: Cadena de texto identificador único del recurso.
Tipo del Recurso: Cadena de texto con categoría del recurso. Use el siguiente dominio en este caso: Testimonio, Informe, Caso.
Cobertura: Combinación entre un rango de fechas y una ubicación.

, fecha de creacion, fecha de ultima actualizacion.


# Tutorial

Proximamente esta disponible un tutorial

DESARROLLADO POR: Bernardo Molina Zuluaga.
![](docs/logo.png)
Correo Electrónico: bermoz@gmail.com

* Visita mi pagina web: (https://www.bermoz.net)

Marzo,  2020
